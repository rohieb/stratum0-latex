include Makefile.include

CLSLIST = s0artcl.cls s0minutes.cls
DTXLIST = s0artcl.dtx s0minutes.dtx
DOCLIST = $(patsubst %.dtx,%.pdf,$(DTXLIST))
INSFILE = stratum0-latex.ins

all: $(CLSLIST) doc

doc: $(DOCLIST)

$(CLSLIST): $(DTXLIST) $(INSFILE)
	$(LATEX_SILENT) $(INSFILE)

%.pdf: %.dtx
	$(ECHO) 'Generating documentation $@ ...'
	$(PDFLATEX_SILENT) $< $(SILENT)
	$(MAKEINDEX) -q -s gind.ist -o $*.ind $*.idx
	$(PDFLATEX_SILENT) $< $(SILENT)
	$(ECHO) -e '$(DONE_STRING)'

cleantemp:
	$(RM) *.aux *.glo *.idx *.out *.ilg *.ind *.log *.out *.toc

clean: cleantemp
	$(RM) *.cls *.pdf
